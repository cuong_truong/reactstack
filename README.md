Sexy React Stack
 |_Docs : presentation file
 |_Source : Source Demo

### 1. Make sure you've installed:
> [Nodejs](https://nodejs.org/en/)

> [Webpack](https://webpack.github.io/docs/installation.html)

### 2. I suggest you using [npm-check-updates](https://www.npmjs.com/package/npm-check-updates) to check dependencies's updates

### 3. Installation
```bash
npm install
```

### 4. Linter to check syntax
```bash
npm run lint
```

### 5. Build
```bash
npm run build
```

### 6. Run in Dev mode (with watch mode)
```bash
npm run dev
```

### 7. Run in Production mode
```bash
npm run prod
```

### Or build and run in Production mode by single command
```bash
npm start
```

### 9. Open http://localhost:8000

### Material Icons from https://design.google.com/icons/