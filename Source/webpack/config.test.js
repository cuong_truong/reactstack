const path = require('path')
const webpack = require('webpack')

const nodeModulesPath = path.join(__dirname, '/../node_modules')

module.exports = { //kind of a copy of your webpack config
  devtool: 'inline-source-map', //just do inline source maps instead of the default
  resolveLoader: { root: nodeModulesPath },

  module: {
    loaders: [{
        test: /\.js$/,
        loader: 'babel',
        exclude: nodeModulesPath,
      }, {
        test: /\.json$/,
        loader: 'json',
      }, {
        test: /\.css$/,
        loader: 'style!css'
      }, {
        test: /\.(sass|scss)$/,
        loader: 'style-loader!css-loader!autoprefixer-loader?{browsers:["last 2 versions", "ie 6-8", "Firefox > 20"]}!sass-loader?outputStyle=expanded&indentedSyntax'
      },

      /* images */
      { test: /\.(jpg|jpeg|gif|png|ico)$/, loader: 'url-loader?limit=12288&name=assets/images/[name].[ext]' },

      /* fonts */
      {
        test: /\.(woff|woff2|eot|ttf|svg).*$/,
        loader: 'url-loader?name=assets/fonts/[name].[ext]'
      }

    ],
    preLoaders: [
      // transpile and instrument only testing sources with isparta
      {
        test: /\.js$/,
        exclude: /(spec\.js$|node_modules|test)/,
        loader: 'isparta'
      }
    ]
  },
  externals: {
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('test'),
      __DEVELOPMENT__: false,
      __LOGGER__: false,
      __DEVTOOLS__: false /* DISABLE redux-devtools HERE */
    })
  ]
}
