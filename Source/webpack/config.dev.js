const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const baseConfig = require('./config.base')
const settings = merge(baseConfig.settings, {
  host: '0.0.0.0',
  port: 8000
})

module.exports = merge(baseConfig, {
  settings: settings,
  devtool: 'source-map',
    entry: {
    app: [
      settings.webpackHotMiddleWare,
      settings.srcPath
    ]
  },
  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: 'style!css'
      },
      {
        test: /\.(sass|scss)$/,
        loader: 'style-loader!css-loader!autoprefixer-loader?{browsers:["last 2 versions", "ie 6-8", "Firefox > 20"]}!sass-loader?outputStyle=expanded&indentedSyntax'
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development'),
      __DEVELOPMENT__: true,
      __LOGGER__: true,
      __DEVTOOLS__: true /* DISABLE redux-devtools HERE */
    })
  ]
})