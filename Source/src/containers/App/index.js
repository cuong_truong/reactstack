import App from './App';

export default App;

export * as appActions from './app.actions';

export const AppRoute = {
  path: '/',
  getComponent(nextState, callback) {
    require.ensure([], (require) => {
      callback(null, App);
    });
  }
};