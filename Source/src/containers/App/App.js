import React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import { Navigation, LanguageSwitcher } from 'components';

export default class App extends React.Component {
  render() {
    return (
      <Grid fluid>
        <Row>
          <Navigation />

          <Grid fluid>
            <LanguageSwitcher />

            <Col className='details'>
                {this.props.children}
            </Col>
          </Grid>
        </Row>
      </Grid>
    );
  }
}