import { USERS_SUCCESS, TOGGLE_APP_DRAWER } from './app.actions';
import { I18N_SUCCESS } from 'components';

/**
 * App reducer
 *
 * @param  {Object} state: App state
 * @param  {Object} action: redux action
 * @return {Object} new App state
 */
export default (state = {}, action = { payload: {} }) => {
  switch (action.type) {
    case TOGGLE_APP_DRAWER:
      return state.set('isDrawerOpen', !state.get('isDrawerOpen'));

    case USERS_SUCCESS:
      return state.mergeDeep({ users: action.payload || {} });

    case I18N_SUCCESS:
      return state.mergeDeep({ localization: action.payload });

    default:
      return state;
  }
};
