import { CALL_API } from 'redux/middlewares/api';

export const USERS_REQUEST = 'USER_REQUEST';
export const USERS_SUCCESS = 'USER_SUCCESS';
export const USERS_FAILURE = 'USER_FAILURE';

/**
 * Fetch users from fake api
 *
 * @return {Object}
 */
const fetchUsers = () => {
  return {
    [CALL_API]: {
      headers: { "Access-Control-Allow-Origin": "*" },
      endpoint: 'http://jsonplaceholder.typicode.com/users',
      method: 'GET',
      types: [USERS_REQUEST, USERS_SUCCESS, USERS_FAILURE]
    }
  };
};

/**
 * Load users
 *
 * @return {[type]}
 */
export const loadUsers = () => {
  return (dispatch, getState) => { // eslint-disable-line no-unused-vars
    return dispatch(fetchUsers());
  };
};

export const TOGGLE_APP_DRAWER = 'TOGGLE_APP_DRAWER';

/**
 * Toggle App Drawer (Left bar)
 *
 * @return {Object}
 */
export const toggleAppDrawer = () => {
  return {
    type: TOGGLE_APP_DRAWER
  };
};
