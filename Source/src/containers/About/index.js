import About from "./About";

export default About;

export const AboutRoute = {
  path: 'about',
  getComponent(nextState, callback) {
    require.ensure([], (require) => {
      callback(null, About);
    });
  }
};