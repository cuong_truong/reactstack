import React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

export default class About extends React.Component {
  render() {
    return (
      <Grid fluid id='About'>
        <Row>
          <div className='header'>About</div>
        </Row>
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
            {this.props.children}
          </Col>
        </Row>
      </Grid>
    );
  }
}
