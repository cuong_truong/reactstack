import Dashboard from "./Dashboard";

export default Dashboard;

export const DashboardRoute = {
  path: 'dashboard',
  getComponent(nextState, callback) {
    require.ensure([], (require) => {
      callback(null, Dashboard);
    });
  }
};