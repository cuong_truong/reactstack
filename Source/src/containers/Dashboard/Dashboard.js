import React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

export default class Dashboard extends React.Component {
  render() {
    return (
      <Grid fluid id='Dashboard'>
        <Row>
          <div className='header'>Dashboard</div>
        </Row>
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
            {this.props.children}
          </Col>
        </Row>
      </Grid>
    );
  }
}
