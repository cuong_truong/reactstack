/**
 * App reducer
 *
 * @param  {Object} state: App state
 * @param  {Object} action: redux action
 * @return {Object} new App state
 */
export default (state = {}, action = { payload: {} }) => {
  switch (action.type) {
    default:
      return state;
  }
};
