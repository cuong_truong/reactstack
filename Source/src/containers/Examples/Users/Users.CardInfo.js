import React from 'react';

export default class CardInfo extends React.Component {
  static propTypes = {
    name: React.PropTypes.string
  }

  static defaultProps = {
    key: 'key',
    name: 'name',
    username: 'userName',
    avatar: 'avatar',
    picture: '/images/pes.jpg'
  }

  render() {
    let { name, username, email, company } = this.props;

    return (
      <div className="user__card-info" style={{margin: '0 0 20px 0'}}>
        <div>Name: {name}</div>
        <div>User Name: {username}</div>
        <div>Email: {email}</div>
        <div>Company: {company.name}</div>
        <div>Website: {email}</div>
      </div>
    );
  }
}
