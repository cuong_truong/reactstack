import { createSelector } from 'libs';

const getUsers = (state) => {
  return state.getIn(['app', 'users']);
};

export default createSelector(
  getUsers,
  (users) => {
    return {
      users: users ? users.toJS() : []
    };
  }
);