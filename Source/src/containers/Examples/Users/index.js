import Users from "./Users";

export default Users;

export const UsersRoute = {
  path: 'users',
  getComponent(nextState, callback) {
    require.ensure([], (require) => {
      callback(null, Users);
    });
  }
};