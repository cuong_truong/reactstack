import React from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-bootstrap';
import { FormattedMessage } from 'react-intl';
import CardInfo from './Users.CardInfo';
import selector from './users.selector';
import RaisedButton from 'material-ui/RaisedButton';
import { appActions } from 'containers/App';

@connect(selector, { loadUsers: appActions.loadUsers })
export default class Users extends React.Component {
  /**
   * Handle load user from API
   * @return {[void]} void function
   */
  handleLoadUsers = () => {
    this.props.loadUsers();
  }

  render() {
    return (
      <Grid fluid id='Users'>
        <Row>
          <div className='header'>
            <FormattedMessage id="users" />
          </div>

          <RaisedButton
            className="right"
            label="Get Users"
            primary={true}
            onTouchTap={this.handleLoadUsers}
          />

        </Row>
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
            {
              this.props.users.map((user) =>
                <CardInfo
                  key={user.username}
                  name={user.name}
                  username={user.username}
                  email={user.email}
                  website={user.website}
                  company={user.company}
                />
              )
            }
          </Col>
        </Row>
      </Grid>
    );
  }
}
