import HelloMessage from "./HelloMessage";

export default HelloMessage;

export const HelloMessageRoute = {
  path: 'hello',
  getComponent(nextState, callback) {
    require.ensure([], (require) => {
      callback(null, HelloMessage);
    });
  }
};