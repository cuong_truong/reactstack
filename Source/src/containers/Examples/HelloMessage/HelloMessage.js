import React from "react";
import { FormattedMessage } from 'react-intl';
import TextField from 'material-ui/TextField';

export default class HelloMessage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: ''
    };
  }

  static propTypes = {
    name: React.PropTypes.string
  }

  static defaultProps = {
    name: 'World!'
  }

  handleInputChange = (event) => {
    this.setState({
      name: event.target.value
    })
  }

  render() {
    return (
      <div>
        <div>
          <h1><b>PROPS</b></h1>
          <h1>
            <FormattedMessage id="hello" values={{name: <b>{this.props.name}</b>}}/>
          </h1>
        </div>

        <br /><hr /><br />

        <div>
          <h1><b>STATE</b></h1>
          <TextField
            hintText="Please enter your name"
            floatingLabelText="Name"
            onChange={this.handleInputChange}
          />
          <h1>
            <FormattedMessage id="hello" values={{name: <b>{this.state.name}</b>}}/>
          </h1>
        </div>
      </div>
    )
  }
}
