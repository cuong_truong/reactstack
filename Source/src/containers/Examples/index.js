import { HelloMessage, HelloMessageRoute } from './HelloMessage';
import { Users, UsersRoute } from './Users';

export {
  HelloMessage,
  HelloMessageRoute,
  Users,
  UsersRoute
};

export const ExamplesRoute = {
  path: 'examples',
  getChildRoutes(partialNextState, callback) {
    require.ensure([], (require) => {
      callback(null, [
        HelloMessageRoute,
        UsersRoute
      ]);
    });
  }
};