import NotFound from './NotFound';

export default NotFound;

export const NotFoundRoute = {
  path: '*',
  getComponent(nextState, callback) {
    require.ensure([], (require) => {
      callback(null, NotFound);
    });
  }
};