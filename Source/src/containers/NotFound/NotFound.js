import React from 'react';

export default class NotFound extends React.Component {
  render() {
    return (
      <div className="container">
        <h1>404 Error</h1>
        <p><em>Not Found</em> </p>
      </div>
    );
  }
}