import React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import { FormattedMessage } from 'react-intl';

export default class Home extends React.Component {
  render() {
    return (
      <Grid fluid id='Home'>
        <Row>
          <div className='header'>
            <FormattedMessage id="home" />
          </div>
          <FormattedMessage id="app.greeting" />
        </Row>
        <Row>
          <Col xs={12} sm={12} md={12} lg={12}>
            {this.props.children}
          </Col>
        </Row>
      </Grid>
    );
  }
}
