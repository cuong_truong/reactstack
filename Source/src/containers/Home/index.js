import Home from "./Home";

export default Home;

export const HomeRoute = {
  path: 'home',
  getComponent(nextState, callback) {
    require.ensure([], (require) => {
      callback(null, Home);
    });
  }
};