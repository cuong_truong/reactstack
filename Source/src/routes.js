import {
  App,
  Home,
  AboutRoute,
  DashboardRoute,
  HomeRoute,
  NotFoundRoute,
  ExamplesRoute
} from './containers';

export const rootRoute = {
  path: '/',
  component: App,
  indexRoute: {
    component: Home
  },
  childRoutes: [
    AboutRoute,
    DashboardRoute,
    HomeRoute,
    ExamplesRoute,
    NotFoundRoute
  ]
};