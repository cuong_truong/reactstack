require("bootstrap/dist/css/bootstrap.css");
require("./assets/styles/app.scss");
require("./assets/images/app.ico");
require("./assets/images/pes.jpg");

import React from 'react';
import Immutable from 'immutable';
import injectTapEventPlugin from 'react-tap-event-plugin';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { render } from "react-dom";
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import syncHistoryWithStore from './redux/history';
import { configureStore } from './redux/store';
import { DevTools, LocalizationProvider } from './components';
import { lightTheme } from './libs';
import { rootRoute } from './routes';

/* react-tap-event-plugin to listen for touch / tap / clickevents */
injectTapEventPlugin();

const initialState = Immutable.fromJS(__INITIAL_STATE__);
const store = configureStore(browserHistory, initialState);
const history = syncHistoryWithStore(browserHistory, store);
const devTools = __DEVTOOLS__ ? <DevTools /> : null;
const muiTheme = getMuiTheme(lightTheme);
const app = (
  <MuiThemeProvider muiTheme={muiTheme}>
    <Provider key="provider" store={store}>
      <LocalizationProvider>
        <div>
          <Router history={history} routes={rootRoute} />
          {devTools}
        </div>
      </LocalizationProvider>
    </Provider>
  </MuiThemeProvider>
);

render(app, document.getElementById('app'));
