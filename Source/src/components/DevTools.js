import { createDevTools } from 'redux-devtools';
import React from 'react';
import DockMonitor from 'redux-devtools-dock-monitor';
import Inspector from 'redux-devtools-inspector'; // eslint-disable-line no-unused-vars

export default createDevTools(
  <DockMonitor
    toggleVisibilityKey='ctrl-h'
    changePositionKey='ctrl-q'
    defaultIsVisible={false}>
    <Inspector supportImmutable={true} />
  </DockMonitor>
);
