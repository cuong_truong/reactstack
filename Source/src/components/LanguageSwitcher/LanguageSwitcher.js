import React from "react";
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { connect } from 'react-redux';
import { switchLanguage } from './languageSwitcher.actions';

@connect(null, { switchLanguage })
export class LanguageSwitcher extends React.Component {
  /**
   * Listen on Localization from <LocalizationProvider />
   * @type {Object}
   */
  static contextTypes = {
    locale: React.PropTypes.string.isRequired
  };

  /**
   * Handle event on change language
   * @param  {[type]} event  [description]
   * @param  {[type]} index  [description]
   * @param  {[type]} locale [selected language]
   * @return {[void]} [void function]
   */
  handleChangeLanguage = (event, index, locale) => {
    this.props.switchLanguage(locale);
  }

  /**
   * Render component
   * @return {[component]} [LanguageSwitcher]
   */
  render() {
    return (
      <div className="text-right">
        <SelectField value={this.context.locale} onChange={this.handleChangeLanguage}>
          {
            __LANGUAGES__.map((language) => {
              return <MenuItem key={language.locale} value={language.locale} primaryText={language.name} />;
            })
          }
        </SelectField>
      </div>
    );
  }
}
