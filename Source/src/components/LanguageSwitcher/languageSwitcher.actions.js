import { CALL_API } from 'redux/middlewares/api';

export const I18N_REQUEST = 'I18N_REQUEST';
export const I18N_SUCCESS = 'I18N_SUCCESS';
export const I18N_FAILURE = 'I18N_FAILURE';

export const switchLanguage = (locale) => {
  return {
    [CALL_API]: {
      endpoint: `/i18n/${locale}`,
      method: 'GET',
      types: [I18N_REQUEST, I18N_SUCCESS, I18N_FAILURE]
    }
  };
};
