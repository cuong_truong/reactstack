import React from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import { appConstants } from 'libs';

export default class NavBar extends React.Component {
  /**
   * ES7 Property Initializers for Prop Types of React component
   * @type {Object}
   */
  static propTypes = {
    onClickMenu: React.PropTypes.func.isRequired
  }

  /**
   * Constructor
   * @param  {[any]} props [construc params]
   * @return {[void]}
   */
  constructor(props) {
    super(props);

    this.languages = __LANGUAGES__ || [];
  }

  /**
   * Handle on touch tap menu icon on nav bar
   * @return {[type]} [description]
   */
  handleLeftIconButtonTouchTap = () => {
    this.props.onClickMenu();
  }

  /**
   * [description]
   * @param  {[type]} locale [description]
   * @return {[type]}        [description]
   */
  handleChangeLanguage = (locale) => {
    this.props.onChangeLanguague(locale);
  }

  /**
   * Implement render() function
   * @return {Component} NavBar component
   */
  render() {
    return (
      <AppBar
        title={appConstants.APP_TITLE}
        iconElementLeft={
          <IconButton
            onClick={::this.handleLeftIconButtonTouchTap}>
            <MenuIcon />
          </IconButton>
        }
        iconElementRight={
          <IconMenu
            iconButtonElement={
              <IconButton><MoreVertIcon /></IconButton>
            }
            targetOrigin={{horizontal: 'right', vertical: 'top'}}
            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
          >
            <MenuItem primaryText={appConstants.REFRESH} />
            <MenuItem primaryText={appConstants.HELP} />
            <MenuItem primaryText={appConstants.SIGN_OUT} />
          </IconMenu>
        }
      />
    );
  }
}