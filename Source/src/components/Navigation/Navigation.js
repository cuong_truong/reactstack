import React from 'react';
import { NavBar, SideBar } from 'components';

export default class Navigation extends React.Component {
  /**
   * Constructor
   * @param  {[any]} props [construct params]
   * @return {[void]}
   */
  constructor(props) {
    super(props);

    this.state = {
      isDrawerOpen: false
    };
  }

  /**
   * Handle toggle app drawer
   * @return {[void]}
   */
  handleToggleDrawer() {
    this.setState({ isDrawerOpen: !this.state.isDrawerOpen });
  }

  render() {
    return (
      <div>
        <NavBar onClickMenu={::this.handleToggleDrawer} />
        <SideBar onRequestChange={::this.handleToggleDrawer} open={this.state.isDrawerOpen} />
      </div>
    );
  }
}