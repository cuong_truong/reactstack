import React from 'react';
import { connect } from 'react-redux';
import Drawer from 'material-ui/Drawer';
import Home from 'material-ui/svg-icons/action/home';
import Dashboard from 'material-ui/svg-icons/action/dashboard';
import InfoOutline from 'material-ui/svg-icons/action/info-outline';
import SettingsEthernet from 'material-ui/svg-icons/action/settings-ethernet';
import SupervisorAccount from 'material-ui/svg-icons/action/supervisor-Account';
import Momitor from 'material-ui/svg-icons/av/remove-from-queue';
import { List, ListItem } from 'material-ui/List';
import { Link } from 'react-router';
import { appConstants, routeConstants } from 'libs';
import { switchLanguage } from './navigation.actions';

@connect(null, { switchLanguage })
export default class SideBar extends React.Component {
  /**
   * [constructor description]
   * @param  {[type]} props [description]
   * @return {[type]}       [description]
   */
  constructor(props) {
    super(props);

    /**
     * Initial drawer menu items
     *
     * @type {Array}
     */
    this.menus = [
      {
        id: appConstants.HOME,
        primaryText: appConstants.HOME,
        icon: <Home />,
        href: routeConstants.URL_HOME
      },

      {
        id: appConstants.DASHBOARD,
        primaryText: appConstants.DASHBOARD,
        icon: <Dashboard />,
        href: routeConstants.URL_DASHBOARD
      },

      {
        id: appConstants.EXAMPLES,
        primaryText: appConstants.EXAMPLES,
        icon: <Momitor />,
        href: null,
        nestedItems: [
          {
            id: appConstants.HELLO,
            primaryText: appConstants.HELLO,
            icon: <SettingsEthernet />,
            href: routeConstants.URL_EXAMPLES_HELLO
          },

          {
            id: appConstants.USERS,
            primaryText: appConstants.USERS,
            icon: <SupervisorAccount />,
            href: routeConstants.URL_EXAMPLES_USERS
          }
        ]
      },

/*      {
        id: appConstants.LANGUAGES,
        primaryText: appConstants.LANGUAGES,
        icon: <Settings />,
        href: null,
        nestedItems: [
          {
            id: appConstants.ENGLISH,
            primaryText: appConstants.ENGLISH,
            icon: <span>EN</span>,
            onTouchTap: this.handleOnChangeLanguage
          },

          {
            id: appConstants.FRENCH,
            primaryText: appConstants.FRENCH,
            icon: <span>FR</span>,
            onTouchTap: this.handleOnChangeLanguage
          },
        ]
      },*/

      {
        id: appConstants.ABOUT,
        primaryText: appConstants.ABOUT,
        icon: <InfoOutline />,
        href: routeConstants.URL_ABOUT
      }
    ];

    this.handleOnChangeLanguage = this.handleOnChangeLanguage.bind(this);
  }

  handleOnChangeLanguage = (locale = 'en') => {
    this.props.switchLanguage(locale);
  }

  /**
   * Render menu item
   * @param  {Object} item: menu item object
   * @param  {Int} index: item's index
   * @return {Component}
   */
  renderDrawerItem = (item, index) => {
    const nestedItems = item.nestedItems || [];
    const containerElement = item.href
      ? <Link key={item.id} to={item.href} activeClassName='active'>{item.primaryText}</Link>
      : <div></div>;

    return (
      <ListItem
        key={item.id || index}
        className='drawer__item'
        primaryText={item.primaryText}
        initiallyOpen={item.initiallyOpen || true}
        primaryTogglesNestedList={item.primaryTogglesNestedList || true}
        disabled={item.disabled || false}
        nestedItems={nestedItems.map((nestedItem, niIndex) => this.renderDrawerItem(nestedItem, niIndex))}
        leftIcon={item.icon}
        containerElement={containerElement}
      />
    );
  };

  /**
   * [render description]
   * @return {Component}
   */
  render() {
    return (
      <Drawer
        docked={false}
        width={250}
        open={this.props.open}
        onRequestChange={this.props.onRequestChange}
        className='drawer'
        containerClassName='drawer-container'
        overlayClassName='drawer__overlay'
      >
        <List className='drawer__menu'>
          {this.menus.map((listItem, index) => this.renderDrawerItem(listItem, index))}
        </List>
      </Drawer>
    );
  }
}
