import './localizationProvider.data';
import React from 'react';
import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl';
import selector from './localizationProvider.selector';

@connect(selector)
export default class LocalizationProvider extends React.Component {
  static childContextTypes = {
    locale: React.PropTypes.string
  };

  /**
   * Pass 'localization' through child components
   * @return {[object]} childContext object
   */
  getChildContext() {
    return { locale: this.props.locale };
  }

  /**
   * Render LocalizationProvider component
   * @return {[component]} [LocalizationProvider]
   */
  render() {
    return <IntlProvider {...this.props} />;
  }
}
