import { createSelector } from 'libs';

const getLocalization = (state) => {
  return state.getIn(['app', 'localization']);
};

export default createSelector(
  getLocalization,
  (localization) => {
    const locale = localization.get('locale');
    const messages = localization.get('messages').toJS();

    return {
      key: locale,
      locale,
      messages
    };
  }
);