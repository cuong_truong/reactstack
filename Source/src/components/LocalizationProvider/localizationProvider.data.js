import { addLocaleData } from 'react-intl';
import itLocaleData from 'react-intl/locale-data/it';
import enLocaleData from 'react-intl/locale-data/en';
import frLocaleData from 'react-intl/locale-data/fr';

addLocaleData([
  ...itLocaleData,
  ...enLocaleData,
  ...frLocaleData
]);
