import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { persistState } from 'redux-devtools';
import createLogger from 'redux-logger';

import { DevTools } from 'components';
import { apiMiddleware } from './middlewares';
import { rootReducer } from './reducers';

/**
 * Create redux store with middleware(s)
 *
 * @param  {Object} history: browser history
 * @param  {Object} initialState: default application state
 * @return {Object}
 */
export const configureStore = (history, initialState) => {
  // Sync dispatched route actions to the history
  const reduxRouterMiddleware = routerMiddleware(history);
  let middleware = [thunk, apiMiddleware, reduxRouterMiddleware];
  let finalCreateStore = null;

  // Case LOGGER is enabled in webpack/config files
  if (__LOGGER__) {
    middleware.push(createLogger());
  }

  // Case DevTools is enabled in webpack/config files
  if (__DEVTOOLS__) {
    finalCreateStore = compose(
      applyMiddleware(...middleware),
      window.devToolsExtension ? window.devToolsExtension() : DevTools.instrument(),
      persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
    )(createStore);
  }
  else {
    finalCreateStore = applyMiddleware(...middleware)(createStore);
  }

  const store = finalCreateStore(rootReducer, initialState);

  if (__DEVELOPMENT__ && module.hot) {
    module.hot.accept('./reducers', () => {
      store.replaceReducer(rootReducer);
    });
  }

  return store;
};