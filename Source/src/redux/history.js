import { syncHistoryWithStore } from 'react-router-redux';

let cachedRouting = null;
let lastRouting = null;

const stateRouting = (state) => state.get('routing');

const getRoutingFromState = (state) => {
  if (!lastRouting || !stateRouting(state).equals(lastRouting)) {
    lastRouting = stateRouting(state);
    cachedRouting = lastRouting.toJS();
  }

  return cachedRouting;
};

export default (browserHistory, store) => {
  return syncHistoryWithStore(browserHistory, store, {
    selectLocationState(state) {
      return getRoutingFromState(state);
    }
  });
};