import {
    combineReducers
} from 'redux-immutable';
import app from 'containers/App/app.reducer';
import routerReducer from './route.reducer';
// import { reducer as reduxAsyncConnect } from 'redux-async-connect'

/**
 * This is root reducer that combines from modules' reducers
 *
 * @type {Object}
 */
export default combineReducers({
  app,
  routing: routerReducer
  // reduxAsyncConnect,
});
