import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import app from 'containers/App/app.reducer';
// import { reducer as reduxAsyncConnect } from 'redux-async-connect'

/**
 * This is root reducer that combines from modules' reducers
 *
 * @type {Object}
 */
export default combineReducers({
  app,
  routing: routerReducer
  // reduxAsyncConnect,
});
