require('babel-register')();
require("babel-polyfill");

// Ensure all files in src folder are loaded for proper code coverage analysis.
const srcContext = require.context('../src', true, /spec\.js$/);
srcContext.keys().forEach(srcContext);
