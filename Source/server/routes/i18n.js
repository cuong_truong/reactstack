'use strict';
const path = require('path');
const fs = require('fs');
const express = require('express');
let router = express.Router();

const messagesFolder = '../resources';

function getData(locale) {
  return require(`${messagesFolder}/${locale}.json`);
}

function existFile(filePath) {
  try {
    return fs.statSync(filePath).isFile();
  } catch (e) {
    return false;
  }
}

function validateLocale(locale) {
  const messagesPath = path.join(__dirname, `/${messagesFolder}/${locale}.json`);
  return /^[a-z]{2}$/.test(locale) && existFile(messagesPath);
}

router
  .get('/:locale', (req, res) => {
    let locale = req.params.locale;
    let messages = null
    if (validateLocale(locale))
      messages = getData(locale);

    res.send({locale, messages});
  });
  
module.exports = router;