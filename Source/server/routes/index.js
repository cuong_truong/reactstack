'use strict';

const express = require('express');
const router = express.Router();

const initialState = {
  app: {
    localization: {
      defaultLocale: 'en',
      locale: 'en',
      messages: require('../resources/en.json')
    }
  }
};

router.get('', (req, res) => {
  res.render('index', { initialState });
});

module.exports = router;