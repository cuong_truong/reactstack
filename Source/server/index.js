const express = require('express')
const compression = require('compression');
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const config = require('../webpack/config')
const i18n = require('./routes/i18n');
const defaultRoute = require('./routes');

const app = express()
const host = config.settings.host || '0.0.0.0'
const port = config.settings.port || 8000
const env = process.env.NODE_ENV || 'development'
const isDevMode = env.toLowerCase() !== 'production'

app.use(compression());

app.set('views', `${__dirname}/views`);
app.set('view engine', 'pug');

/* Case DEV mode */
if (isDevMode) {
  const compiler = webpack(config)
  app.use(webpackHotMiddleware(compiler))
  app.use(webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    stats: {
      colors: true,
      timings: true,
    }
  }))
}
/* Case PRODUCTION mode */
else {
  app.use(express.static(config.settings.distPath));
}

//Add route handlers
app.use('/i18n', i18n);
app.use('*', defaultRoute);

app.listen(port, config.settings.host, function(error) {
  if (error) {
    console.info("⛔ ⛔ ⛔  *** ERROR *** ⛔ ⛔ ⛔")
    console.error(error)
  } else {
    console.info("✅ ✅ ✅  *** %s mode's started *** ✅ ✅ ✅", env.toUpperCase())
    console.info("✅ ✅ ✅  *** Listening at http://%s:%s *** ✅ ✅ ✅", host, port)
  }
})
