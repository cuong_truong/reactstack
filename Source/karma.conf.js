const webpack = require('webpack')
var path = require('path');
var webpackConfig = require('./webpack/config.test')

module.exports = function(config) {
  config.set({
    frameworks: ['mocha'],
    files: [
      {pattern:'src/**/!(spec).js', included: false, served: false},
      'test/index.js'
    ],

    preprocessors: {
      'test/index.js': ['webpack', 'sourcemap']
    },

    webpack: webpackConfig,
    webpackServer: {
      noInfo: true //please don't spam the console when running in karma!
    },

    reporters: ['progress', 'coverage'],
    coverageReporter: {
      type: 'html',
      dir: 'test/coverage',
    },

    port: 9876,
    colors: true,
    autoWatch: true,

    logLevel: config.LOG_INFO,
    browsers: ['PhantomJS'],
    singleRun: true,

    /* DEBUG */
    // browsers: ['Chrome'],
    // singleRun: false,
    // logLevel: config.LOG_DEBUG,
    
  })
};
